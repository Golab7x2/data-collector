import { Builder, By } from 'selenium-webdriver';
import { ServiceBuilder } from 'selenium-webdriver/chrome.js';
import ENV from './env.js';
import { getAllTickers } from './excel-handler.js';
import { appendFile } from 'fs';

const serviceBuilder = new ServiceBuilder(ENV.webdriver);

async function run() {
    const driver = await new Builder()
    .forBrowser('chrome')
    .setChromeService(serviceBuilder)
    .build();
    const tickers = await getAllTickers();
    try {
        await driver.get(ENV.marketCapURL);
        const acceptCookies = await driver.findElement(By.xpath('html/body/div/div/div/div/form/div[2]/div[2]/button'));
        await acceptCookies.click();

        for (let t of tickers) {
            await driver.get(`${ENV.marketCapURL}${t}`);
            const marketCap = await driver.findElement(By.xpath('/html/body/div[1]/div/div/div[1]/div/div[3]/div[1]/div/div[1]/div/div/div/div[2]/div[2]/table/tbody/tr[1]/td[2]/span'))
                .catch(() => console.log(`Cannot read Market Capitalization for ${t}`));
            if (marketCap) {
                const marketCapValue = await marketCap.getText();
            const convertedMarketCapValue = convertMarketCap(marketCapValue);
            console.log(`${t}: ${convertedMarketCapValue}\t\tProgress: (${(((tickers.indexOf(t)+1)/tickers.length)*100).toFixed(2)}%)`);
            appendFile('./data/market-cap-retake.txt', `${t}\t${convertedMarketCapValue}\r\n`, err => {
                if (err) console.log(err)});
            } else {
                appendFile('./data/market-cap-retake.txt', `${t}\tCannot read Market Capitalization value\r\n`, err => {
                    if (err) console.log(err)});
                continue;
            };
            
        }
    } catch (error) {
        console.log(error);
    }
}

function convertMarketCap(value) {
    const lastChar = value.charAt(value.length-1);
    switch (lastChar) {
        case 'M':
            return Math.round(value.substring(0,value.length-1)*1000000);
        case 'B':
            return Math.round(value.substring(0,value.length-1)*1000000000);
        case 'T':
            return Math.round(value.substring(0,value.length-1)*1000000000000);
        default:
            return value;
    }
}

run();