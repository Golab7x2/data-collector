import { Builder, By, Key, until } from 'selenium-webdriver';
import { ServiceBuilder } from 'selenium-webdriver/chrome.js';
import ENV from './env.js';
import { appendFile } from 'fs';

const serviceBuilder = new ServiceBuilder(ENV.webdriver);

async function run() {
    const driver = await new Builder()
    .forBrowser('chrome')
    .setChromeService(serviceBuilder)
    .build();

    try {
        await driver.get('https://www.nyse.com/listings_directory/stock');

        for (let i = 1; i <= 756; i++) {
            console.log(`Page: ${i}\t\t${((i/756)*100).toFixed(2)}%`);
            await sleep(3000);
            const table = await driver.findElement(By.xpath('/html/body/div[3]/div[4]/div[1]/div/div[2]/div[1]/table/tbody'));
            //console.log(`LOG: ${await table.getText()}`);

            const tickers = await table.findElements(By.xpath('//tr/td/a'));
            for (let t of tickers) {
                const innerText = await t.getText();
                appendFile('./data/nyse-tickers.txt', `${innerText}\r\n`, err => {
                    if (err) console.log(err)});
            }
        
            const nextButton = await driver.findElement(By.xpath('/html/body/div[3]/div[4]/div[1]/div/div[2]/div[2]/div/ul/li[8]/a'));
            nextButton.click();
        }
        
    } catch (error) {
        console.log(error);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

run();