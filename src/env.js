const ENV = {
    webdriver: './chromedriver_win32/chromedriver.exe',
    patentsURL: 'https://patft.uspto.gov/netahtml/PTO/search-bool.html',
    marketCapURL: 'https://finance.yahoo.com/quote/'
}

export default ENV;