import { Builder, By } from 'selenium-webdriver';
import { ServiceBuilder } from 'selenium-webdriver/chrome.js';
import exceljs from 'exceljs';
import ENV from './env.js';
import { appendFile } from 'fs';

const serviceBuilder = new ServiceBuilder(ENV.webdriver);
const outputFile = './data/patents-fixed-take4.csv';
const header = 'Company,2011,2012,2013,2014,2015,2016,2017,2018,2019\r\n';

async function run() {
    const driver = await new Builder()
    .forBrowser('chrome')
    .setChromeService(serviceBuilder)
    .build();

    const companies = await excelCompaniesRead();
    console.log(`All companies: ${companies.length}`);
    appendFile(
    outputFile,
    header,
     err => {if (err) console.log(err)}
    );
    
    for(let company of companies) {
        let safetyCatch = false;

        try {
            console.log(`Progress: ${(((companies.indexOf(company)+1)/companies.length)*100).toFixed(2)}%`);
            await readPatents(driver, company); 
        } catch (error) {
            console.log(`Failed at: ${company}`);
            safetyCatch = true;
            appendFile(
                outputFile,
                `${company},FAILED TO COLLECT DATA,\r\n`,
                 err => {if (err) console.log(err)}
                );
        }

        if(safetyCatch) {
            await sleep(5000);
            console.log('Retaking...');
        }
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function excelCompaniesRead() {
    const path = './data/companies_name_take4.xlsx';

    const wb = new exceljs.Workbook();
    await wb.xlsx.readFile(path);

    const sheet = wb.worksheets[0];

    const companies = [];
    sheet
        .getRows(1, sheet.lastRow.number)
        .forEach(r => companies.push(r.getCell('B').value));

    return companies;
}

async function readPatents(driver, company) {

    let toSave = `${company},`;
    for (let i = 2011; i <= 2019; i++) {
        await driver.get('https://patft.uspto.gov/netahtml/PTO/search-bool.html');
        // trm1 - fld1
        const input1 = await driver.findElement(By.id('trm1'));
        input1.sendKeys(company);

        const select1 = await driver.findElement(By.id('fld1'));
        const value1 = await select1.findElement(By.css('option[value="ASNM"]'));
        await value1.click();

        // trm2 - fld2
        const input2 = await driver.findElement(By.id('trm2'));
        input2.sendKeys(i);

        const select2 = await driver.findElement(By.id('fld2'));
        const value2 = await select2.findElement(By.css('option[value="ISD"]'));
        await value2.click();

        const searchButton = await driver.findElement(By.css('input[type="SUBMIT"]'));
        await searchButton.click();

        const title = await driver.getTitle();
        
        let numberOfPatents = 0;
        if (title.includes('Single Document')) {
            numberOfPatents = 1;
        } else {
            const i = await driver.findElements(By.css('i'));
            const info = await i[i.length-1].getText();
            const separatedInfo = info.split(' ');
            const lastValue = separatedInfo[separatedInfo.length-1];
            lastValue !== '...' ? numberOfPatents = lastValue : numberOfPatents;
        }

        //console.log(numberOfPatents);
        toSave = toSave.concat(`${numberOfPatents},`);
    }
    appendFile(outputFile, `${toSave}\r\n`, err => {
        if (err) console.log(err)});
}

run();
