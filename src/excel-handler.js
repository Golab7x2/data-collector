import exceljs from 'exceljs';

export async function getAllTickers() {
    const path = './data/Companies_SIC_Codes.xlsx';

    const wb = new exceljs.Workbook();
    await wb.xlsx.readFile(path);

    const tickers = [];
    wb.worksheets.forEach(sheet => {
        const rows = sheet.getRows(2, sheet.lastRow.number);
        rows.forEach(r => tickers.push(r.getCell('B').value))
    });

    return tickers.slice(0, 1700);
}